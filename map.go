package noise

import (
	"container/list"
	"errors"
	"fmt"
	"math"
	"sync"
)

type clientMapEntry struct {
	el     *list.Element
	client *Client
}

type clientMap struct {
	cap     uint
	order   *list.List
	entries map[string]clientMapEntry
}

func newClientMap(cap uint) *clientMap {
	return &clientMap{
		cap:     cap,
		order:   list.New(),
		entries: make(map[string]clientMapEntry, cap),
	}
}

func (n *Node) clientMapHandler() {
	n.Log("info", fmt.Sprintf("starting client map handler for node: %s", n.id.String()))

	// having two handlers might be a bit more effecient.
	inMap := newClientMap(n.maxInboundConnections)
	outMap := newClientMap(n.maxOutboundConnections)

	for {
		select {
		case clientReqs := <-n.cMapIn:
			n.Log("info", fmt.Sprintf("got clients request in clientMap for clients: %v", clientReqs))

			clientResp := make([]*Client, len(clientReqs))

			for i, addr := range clientReqs {
				var found = false
				for _, entry := range inMap.entries {
					if entry.client.id.Address == addr {
						inMap.order.MoveToFront(entry.el)
						clientResp[i] = entry.client
						found = true
						break
					}
				}

				if found {
					continue
				}

				for _, entry := range outMap.entries {
					if entry.client.id.Address == addr {
						outMap.order.MoveToFront(entry.el)
						clientResp[i] = entry.client
						break
					}
				}

			}

			n.Log("info", fmt.Sprintf("got %d clients for addrs:  %v", len(clientReqs), clientReqs))

			n.cMapOut <- clientResp
			continue
		case newWrap := <-n.cMapAddInbound:
			n.Log("info", fmt.Sprintf("got new inbound client: %s to add to inMap", newWrap.addr))

			entry, exists := inMap.entries[newWrap.addr]
			if !exists {
				n.Log("info", fmt.Sprintf("client: %s doesn't exist in inMap", newWrap.addr))

				if uint(len(inMap.entries)) >= n.maxInboundConnections {
					n.Log("warn", fmt.Sprintf("inMap is too full (%d enteries out of %d max. Removing oldest", len(outMap.entries), n.maxOutboundConnections))
					el := inMap.order.Back()
					evicted := inMap.order.Remove(el).(string)

					e := inMap.entries[evicted]
					delete(inMap.entries, evicted)
					e.client.close()
					n.Log("warn", fmt.Sprintf("removed old client: %s (%s) from inMap", e.client.addr, e.client.id.ID.String()))
				}

				newClient := newClient(n)
				newEntry := clientMapEntry{
					el:     inMap.order.PushFront(newWrap.addr),
					client: newClient,
				}
				inMap.entries[newWrap.addr] = newEntry
				// added new client to map, init client
				n.Log("info", fmt.Sprintf("initing inbound on new client: %s", newWrap.addr))
				go newClient.inbound(n, newWrap.conn, newWrap.addr)

			} else {
				// client is already in map, bump to top
				inMap.order.MoveToFront(entry.el)
			}

		case newWrap := <-n.cMapAddOutbound:
			n.Log("info", fmt.Sprintf("got new outbound client to add to inMap"))

			entry, exists := outMap.entries[newWrap.addr]
			if !exists {
				n.Log("info", fmt.Sprintf("client: %s doesn't exist in outMap, adding it", newWrap.addr))

				if uint(len(outMap.entries)) >= n.maxOutboundConnections {
					n.Log("warn", fmt.Sprintf("outMap is too full (%d enteries out of %d max. Removing oldest", len(outMap.entries), n.maxOutboundConnections))
					el := outMap.order.Back()
					evicted := outMap.order.Remove(el).(string)

					e := outMap.entries[evicted]
					delete(outMap.entries, evicted)
					e.client.close()
					n.Log("warn", fmt.Sprintf("removed old client: %s (%s) from outMap", e.client.addr, e.client.id.ID.String()))
				}

				newClient := newClient(n)
				newEntry := clientMapEntry{
					el:     outMap.order.PushFront(newWrap.addr),
					client: newClient,
				}

				outMap.entries[newWrap.addr] = newEntry

				// added new entry, initalize new client
				n.Log("info", fmt.Sprintf("initing outbound on new client: %s", newWrap.addr))
				go newClient.outbound(newWrap.ctx, newWrap.addr)

				n.cMapAddOutboundResp <- newClient
			} else {
				// it's already in the map, just bump to top of list and ignore
				n.Log("info", fmt.Sprintf("entry with addr: %s already exists in outMap. Bumping to top and returning", newWrap.addr))
				outMap.order.MoveToFront(entry.el)
				n.cMapAddOutboundResp <- entry.client
			}

		case remClients := <-n.cMapRem:
			n.Log("info", fmt.Sprintf("got clients to remove from clientMap: %s", remClients))

			for _, addr := range remClients {
				for _, entry := range outMap.entries {
					if entry.client.addr == addr {
						n.Log("info", fmt.Sprintf("removing client with addr from outMap: %s", addr))
						outMap.order.Remove(entry.el)
						delete(outMap.entries, addr)
					}
				}

				// remove from both
				for _, entry := range inMap.entries {
					if entry.client.addr == addr {
						n.Log("info", fmt.Sprintf("removing client with addr from inMap: %s", addr))
						inMap.order.Remove(entry.el)
						delete(inMap.entries, addr)
					}
				}
				n.Log("info", fmt.Sprintf("finished removing client: %s from clientMaps", addr))
			}

		case bound := <-n.cMapBoundIn:
			// bound = true, get outbound clients
			// bound = false, get inbound clients
			n.Log("info", fmt.Sprintf("got bound request with bound = %v", bound))

			var clientResp []*Client

			if bound {
				for _, entry := range outMap.entries {
					clientResp = append(clientResp, entry.client)
				}
				n.Log("info", fmt.Sprintf("returning %d outbound clients", len(clientResp)))
			} else {
				for _, entry := range inMap.entries {
					clientResp = append(clientResp, entry.client)
				}
				n.Log("info", fmt.Sprintf("returning %d inbound clients", len(clientResp)))
			}

			n.cMapBoundOut <- clientResp

		case _ = <-n.cMapTerm:
			n.Log("info", fmt.Sprintf("terminating clientMaps and shutting down clientMapHandler"))

			entries := outMap.entries
			outMap.entries = make(map[string]clientMapEntry, n.maxOutboundConnections)

			for _, e := range entries {
				e.client.close()
			}

			entries = inMap.entries
			inMap.entries = make(map[string]clientMapEntry, n.maxInboundConnections)

			for _, e := range entries {
				e.client.close()
			}

			n.Log("info", fmt.Sprintf("finished terminating cMapHandler"))
			return // exit the handler
		}
	}
}

type requestMap struct {
	sync.Mutex
	entries map[uint64]chan message
	nonce   uint64
}

func newRequestMap() *requestMap {
	return &requestMap{entries: make(map[uint64]chan message)}
}

func (r *requestMap) nextNonce() (<-chan message, uint64, error) {
	r.Lock()
	defer r.Unlock()

	if r.nonce == math.MaxUint64 {
		r.nonce = 0
	}

	r.nonce++
	nonce := r.nonce

	if _, exists := r.entries[nonce]; exists {
		return nil, 0, errors.New("ran out of available nonce to use for making a new request")
	}

	ch := make(chan message, 1)
	r.entries[nonce] = ch

	return ch, nonce, nil
}

func (r *requestMap) markRequestFailed(nonce uint64) {
	r.Lock()
	defer r.Unlock()

	close(r.entries[nonce])
	delete(r.entries, nonce)
}

func (r *requestMap) findRequest(nonce uint64) chan<- message {
	r.Lock()
	defer r.Unlock()

	ch, exists := r.entries[nonce]
	if exists {
		delete(r.entries, nonce)
	}

	return ch
}

func (r *requestMap) close() {
	r.Lock()
	defer r.Unlock()

	for nonce := range r.entries {
		close(r.entries[nonce])
		delete(r.entries, nonce)
	}
}
